module.exports = {
    'transpileDependencies': [
        'vuetify'
    ],
    pwa: {
        name: 'Freespeech',
        themeColor: '#4DBA87',
        msTileColor: '#000000',
        appleMobileWebAppCapable: 'yes',
      },
    publicPath: process.env.NODE_ENV === 'production'
    ? '/<project-name>/'
    : '/'
};